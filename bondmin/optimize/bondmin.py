from bondmin.optimize.aidmin import AIDMin
from bondmin.optimize.scipyopt import SP
from bondmin.gp.calculator import GPCalculator
from bondmin.gp.kernel import BondExponential

from ase.data import covalent_radii, atomic_numbers

import warnings


class BondMin(AIDMin):
    """
    BondMin optimizer. 
    Optimize atomic positions using GPMin algorithm, which uses both
    potential energies and forces information to build a PES via Gaussian
    Process (GP) regression and then minimizes it.

    Default behaviour:
    --------------------
    The default values of the scale, noise, weight, batch_size and bounds
    parameters depend on the value of update_hyperparams. In order to get
    the default value of any of them, they should be set up to None.
    Default values are:

    update_hyperparams = True
        scale : 0.2
        noise : 0.01
        weight: 2.
        bounds: 0.1
        batch_size: 1

    update_hyperparams = False
        scale : 0.4
        noise : 0.0025
        weight: 1.
        bounds: irrelevant
        batch_size: irrelevant


    By default, the full memory version is used. The light memory version can 
    be enabled by setting max_data to an integer. This functionality reduces 
    the number of point in the training set to those who are closest to 
    the test point and does not fit to the forces of constrained atoms.   


    Parameters:
    ------------------

    atoms: Atoms object
        The Atoms object to relax.

    restart: string
        Pickle file used to store the training set. If set, file with
        such a name will be searched and the data in the file incorporated
        to the new training set, if the file exists.

    logfile: file object or str
        If *logfile* is a string, a file with that name will be opened.
        Use '-' for stdout

    trajectory: string
        Pickle file used to store trajectory of atomic movement.

    master: boolean
        Defaults to None, which causes only rank 0 to save files. If
        set to True, this rank will save files.

    force_consistent: boolean or None
        Use force-consistent energy calls (as opposed to the energy
        extrapolated to 0 K). By default (force_consistent=None) uses
        force-consistent energies if available in the calculator, but
        falls back to force_consistent=False if not.

    noise: float
        Regularization parameter for the Gaussian Process Regression.

    weight: float
        Prefactor of the Squared Exponential kernel.
        If *update_hyperparams* is False, changing this parameter
        has no effect on the dynamics of the algorithm.

    update_prior_strategy: string
        Strategy to update the constant from the ConstantPrior
        when more data is collected. It does only work when
        Prior = None

        options:
            'maximum': update the prior to the maximum sampled energy
            'init' : fix the prior to the initial energy
            'average': use the average of sampled energies as prior

    scale: float
        global scale of the method

    update_hyperparams: boolean
        Update hyperparameters of the kernel.

    batch_size: int
        Number of new points in the sample before updating
        the hyperparameters.
        Only relevant if the optimizer is executed in update_hyperparams
        mode: (update_hyperparams = True)

    bounds: float, 0<bounds<1
        Set bounds to the optimization of the hyperparameters.
        Let t be a hyperparameter. Then it is optimized under the
        constraint (1-bound)*t_0 <= t <= (1+bound)*t_0
        where t_0 is the value of the hyperparameter in the previous
        step.
        If bounds is False, no constraints are set in the optimization of
        the hyperparameters.

    max_train_data (default: None)
        maximum number of points in the training set for light memory.
        If None, it is infinetly many points.

    .. warning:: The memory of the optimizer scales as O(n²N²) where
                 N is the number of atoms and n is max_data.
                 If the number of atoms is sufficiently high, this
                 may cause a memory issue for a given max_data.
                 If the number of points in the training set is not
                 restricted, this class prints a warning if the user tries to
                 run BonfMin with more than 100 atoms in the unit cell.
    """

    def __init__(self, atoms, restart=None, logfile='-', trajectory=None,
                 master=None, noise=None, weight=None,
                 scale=None, force_consistent=None, batch_size=None,
                 bounds=None, update_prior_strategy=None,
                 update_hyperparams=True,
                 max_train_data=None):

     
        # 1. Warn the user if the number of atoms is very large
        if max_train_data is None and len(atoms) > 100:
            warning = ('Possible Memeroy Issue. There are more than '
                       '100 atoms in the unit cell. The memory '
                       'of the process will increase with the number '
                       'of steps, potentially causing a memory issue. '
                       'Consider using a different optimizer.')

            warnings.warn(warning)

        # 2. Define interaction and kernel
        def inv_sqsum(A, B):
            rA = covalent_radii[atomic_numbers[A]]
            rB = covalent_radii[atomic_numbers[B]]
            return 4 * (rA + rB)**(-2)

        kernel = BondExponential()
        symbols = atoms.get_chemical_symbols()
        kernel.init_metric(symbols, inv_sqsum)


        # 3. Define default hyperparameters
        #  3.A Updated BondedMin
        if update_hyperparams:
            if scale is None:
                scale = 0.2
            if noise is None:
                noise = 0.01
            if weight is None:
                weight = 2.

            if bounds is None:
                bounds = 0.1
            elif bounds is False:
                bounds = None

            if batch_size is None:
                batch_size = 1

            if update_prior_strategy is None:
                update_prior_strategy = 'fit'
            fit_weight = True

            # Add the weight and the bond hyperparameters to update
            params_to_update = {'weight': bounds}
            for param in kernel.params.keys():
                if param.startswith('l_'):
                    params_to_update[param] = bounds

        #  3.B Un-updated BondedMin
        else:
            if scale is None:
                scale = 0.4
            if noise is None:
                noise = 0.0025
            if weight is None:
                weight = 1.

            if bounds is not None:
                warning = ('The paramter bounds is of no use '
                           'if update_hyperparams is False. '
                           'The value provided by the user '
                           'is being ignored.')
                warnings.warn(warning, UserWarning)
            if batch_size is not None:
                warning = ('The paramter batch_size is of no use '
                           'if update_hyperparams is False. '
                           'The value provived by the user '
                           'is being ignored.')
                warnings.warn(warning, UserWarning)

            # Set batch_size to 1 anyways
            batch_size = 1
            params_to_update = []

            fit_weight = False

            if update_prior_strategy is None:
                update_prior_strategy = 'maximum'


        # 4. Light memory version
        if max_train_data is not None:
            mask_constraints = True
        else:
            mask_constraints = False


        # 5. Set GP calculator
        gp_calc = GPCalculator(train_images=None, noise=noise,
                               params={'weight': weight,
                                       'scale': scale},
                               update_prior_strategy=update_prior_strategy,
                               calculate_uncertainty=False,
                               prior=None, kernel=kernel,
                               params_to_update=params_to_update,
                               batch_size=batch_size,fit_weight=fit_weight,
                               max_train_data=max_train_data,
                               max_data_strategy='nearest_observations',
                               mask_constraints=mask_constraints)

        # 6. Initialize AIDMin under this set of parameters
        AIDMin.__init__(self, atoms, restart=restart, logfile=logfile,
                        trajectory=trajectory, master=master,
                        force_consistent=force_consistent,
                        model_calculator=gp_calc, optimizer=SP,
                        use_previous_observations=False,
                        surrogate_starting_point='min',
                        trainingset=[], print_format='ASE',
                        fit_to='calc',
                        optimizer_kwargs={'fmax': 'scipy default',
                                          'method': 'L-BFGS-B'})
