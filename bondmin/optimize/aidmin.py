from ase.optimize.optimize import Optimizer
from ase.optimize.bfgslinesearch import BFGSLineSearch

from bondmin.gp.calculator import GPCalculator
from bondmin.utils.trainingset import TrainingSet

from ase.calculators.calculator import compare_atoms


class AIDMin(Optimizer):

    def __init__(self, atoms, restart=None, logfile='-', trajectory=None,
                 master=None, force_consistent=None, model_calculator=None,
                 optimizer=BFGSLineSearch, use_previous_observations=False,
                 surrogate_starting_point='min', trainingset=None,
                 print_format='ASE', fit_to='calc', optimizer_kwargs=None,
                 low_memory=False):
        """
        Artificial Intelligence-Driven energy Minimizer (AID-Min) algorithm.
        Optimize atomic structure using a surrogate machine learning
        model [1,2]. Atomic positions, potential energies and forces
        information are used to build a modelled potential energy surface (
        PES) that can be optimized to obtain new suggested structures
        towards finding a local minima in the targeted PES.

        [1] E. Garijo del Rio, J. J. Mortensen and K. W. Jacobsen.
        arXiv:1808.08588.
        [2] J. A. Garrido Torres, E. Garijo del Rio, A. H. Larsen,
        V. Streibel, J. J. Mortensen, M. Bajdich, F. Abild-Pedersen,
        K. W. Jacobsen, T. Bligaard. (submitted).

        Parameters
        --------------

        atoms: Atoms object
            The Atoms object to relax.

        restart: string
            Pickle file used to store the training set. If set, file with
            such a name will be searched and the data in the file incorporated
            to the new training set, if the file exists.

        logfile: file object or str
            If *logfile* is a string, a file with that name will be opened.
            Use '-' for stdout

        trajectory: string
            Pickle file used to store trajectory of atomic movement.

        master: boolean
            Defaults to None, which causes only rank 0 to save files. If
            set to True, this rank will save files.

        force_consistent: boolean or None
            Use force-consistent energy calls (as opposed to the energy
            extrapolated to 0 K). By default (force_consistent=None) uses
            force-consistent energies if available in the calculator, but
            falls back to force_consistent=False if not.

        DOCUMENTATION MISSING!!

        use_previous_observations: boolean
            If False. The optimization starts from scratch.
                If the observations were saved to a trajectory file,
                it is overwritten. If they were kept in a list, they are
                deleted.
            If True. The optimization uses the information that was already
                in the training set that is provided in the optimization.

        surrogate_starting_point: string
               Where to start the minimization from.
               options:
                     'last' : start minimization of the surrogate from the
                              last position
                     'min': start minimzation from the position with
                              the lowest energy of those visited. This option
                              is the one chosen in GPMin [1] and is the
                              recommended one to avoid extrapolation.

        trainingset: None, trajectory file or list
            Where the training set is kept, either saved to disk in a
            trajectory file or kept in memory in a list.
            options:
                None (default):
                    A trajectory file named *trajectory*_observations.traj is
                    automatically generated and the training set is saved to
                    it while generated.
                str: trajectory filename where to append the training set
                list: list were to append the atoms objects.

        print_format: string
            Printing format. It chooses how much information and in which
            format is shown to the user.

            options:
                  'ASE' (default): information printed matches other ASE
                      functions outside from the AID module. ML is transparent
                      to the user.
                  'AID': Original format of the AID module. More informative
                      in respect of ML process.
                      This option is advised for experienced users.
        fit_to: string
            Characteristics of the constraints in the training set.

            options:
                'calc' (default): fit to the output of the calculator, then
                    run over the constrained surface.
                'constraints': fit to the constrained atoms directly

        optimizer_kwargs: dict
            Dictionary with key-word arguments for the surrogate potential.
        """
        if print_format == 'AID':
            if logfile == '-':
                logfile = None
            self.print_format = 'AID'
        else:
            self.print_format = 'ASE'

        self.low_memory = low_memory

        # Initialize the optimizer class
        Optimizer.__init__(self, atoms, restart, logfile, trajectory,
                           master, force_consistent)

        # Model calculator:
        self.model_calculator = model_calculator
        # Default GP Calculator parameters if not specified by the user
        if model_calculator is None:
            self.model_calculator = GPCalculator(
                train_images=[],
                params={'scale': 0.3, 'weight': 2.},
                noise=0.003, update_prior_strategy='fit',
                max_train_data_strategy='nearest_observations',
                max_train_data=5, calculate_uncertainty=False)

        if hasattr(self.model_calculator, 'print_format'):
            self.model_calculator.print_format = print_format

        # Active Learning setup (single-point calculations).
        self.function_calls = 0
        self.force_calls = 0

        self.optimizer = optimizer
        if optimizer_kwargs is not None:
            self.optkwargs = optimizer_kwargs.copy()
        else:
            self.optkwargs = {}
        self.start = surrogate_starting_point

        self.constraints = atoms.constraints.copy()

        # Initialize training set
        if trainingset is None:
            trajectory_main = trajectory.split('.')[0]
            self.train = TrainingSet(trajectory_main + '_observations.traj',
                                     use_previous_observations=False)
        elif isinstance(trainingset, TrainingSet):
            self.train = trainingset
        else:
            self.train = TrainingSet(trainingset,
                         use_previous_observations=use_previous_observations)

        # Make the training set an observer for the run
        self.attach(self.train, atoms=atoms, method='min')

        # Default parameters for the surrogate optimizer
        if 'logfile' not in self.optkwargs.keys():
            self.optkwargs['logfile'] = None
        if 'trajectory' not in self.optkwargs.keys():
            self.optkwargs['trajectory'] = None

        # Define what to fit to
        if fit_to not in ('calc', 'constraints'):
            raise ValueError("fit_to must be either 'calc' or 'constraints'.")
        self.fit_to = fit_to

    def set_trainingset(self, trainingset, substitute=True, atoms=None):
        """
        Sets a new TrainingSet object to store the training set
        Parameters:
        -----------
        trainingset: TrainingSet object
            training set to be attached to the Optimizer
        substitute: bool
            whether to remove the previous Training Set from the
            observers list or not
        atoms: Atoms object or None
           Atoms object to be attached. If None, it is the same
           as in the optimizer
        """

        if atoms is None:
            atoms = self.atoms

        self.train = trainingset

        # Remove previous training set
        def isTrainingSet(observer):
            try:
                return observer[0].__qualname__.startswith('TrainingSet')
            except AttributeError:
                return False

        if substitute:
            self.observers[:] = [obs for obs in self.observers
                                 if not isTrainingSet(obs)]

        # Attach new training set
        self.attach(trainingset, atoms=atoms, method='min')

    def run(self, fmax=0.05, steps=None, ml_steps=500, ml_fmax=None):

        """
        Executing run will start the optimization process.
        This method will return when the forces on all individual
        atoms are less than *fmax* or when the number of steps exceeds
        *steps*.

        Parameters
        ----------
        fmax : float
            Convergence criteria (in eV/Angstrom)

        steps: int
            Maximum number of steps

        ml_steps: int
            Maximum number of steps for the surrogate

        Returns
        -------
        True if the optimizer found a structure with
        fmax below the threshold in nsteps or less.
        False otherwise.
        """

        # Machine Learning parameters
        self.ml_steps = ml_steps

        # First set fmax according to __init__method
        self.ml_fmax = self.optkwargs.get('fmax', None)

        # Now correct it with user define choice in run
        if ml_fmax is not None:
            self.ml_fmax = ml_fmax

        # Finally, especify default option
        if self.ml_fmax is None:
            self.ml_fmax = 0.01 * fmax

        if self.start == 'min':
            self.check()

        Optimizer.run(self, fmax, steps)

    def step(self):

        """
        Inner function over which Optimizer.run iterates
        """

        # 1. Gather observations in every iteration.
        # This serves to use the previous observations (useful for continuing
        # calculations and/or parallel runs).
        train_images = self.train.load_set()

        # Remove constraints.
        for img in train_images:
            if self.fit_to == 'calc':
                img.constraints = []
            elif self.fit_to == 'constraints':
                img.set_constraint(self.constraints)

        # 2. Update model calculator.
        self.model_calculator.update_train_data(train_images=train_images,
            test_images=[self.atoms.copy()])

        # 3. Optimize the structure in the predicted PES
        self.min_surrogate(self.atoms)

        # 4. Evaluate the target function
        self.atoms.get_potential_energy(force_consistent=self.force_consistent)
        self.atoms.get_forces()
        self.function_calls = len(train_images) + 1
        self.force_calls = self.function_calls

        # 5. Check if the performance is sensible
        if self.start == 'min':
            self.check()

    def min_surrogate(self, atoms):

        # Setup atoms obejct in surrogate potential
        ml_atoms = atoms.copy()
        ml_atoms.set_calculator(self.model_calculator)
        if self.start == 'min':
            ml_atoms.set_positions(self.p0)

        # Set constraints
        if self.fit_to == 'constraints':
            ml_atoms.set_constraint([])

        # kwargs does not know fmax
        kwargs = self.optkwargs.copy()
        if 'fmax' in kwargs:
            del kwargs['fmax']

        def optimize():
            # Optimize
            opt = self.optimizer(ml_atoms, **kwargs)
            ml_converged = opt.run(fmax=self.ml_fmax, steps=self.ml_steps)

            if not ml_converged:
                raise RuntimeError(
                    "The minimization of the surrogate PES has not converged")
            else:
                # Check that this was a meaningfull step
                system_changes = compare_atoms(atoms, ml_atoms)

                if not system_changes:
                    raise RuntimeError("Too small step: the atoms did not move")

        # Optimize
        optimize()
        
        # Initialize convergence flag for including points in data set
        data_converged = False if self.low_memory else True
        probed_atoms = [atoms.copy()]
        while not data_converged:
            probed_atoms.append(ml_atoms.copy())
            ml_atoms._calc.update_train_data(train_images=[],
                test_images=probed_atoms)
            optimize()
            p1 = probed_atoms[-1].get_positions()
            p0 = ml_atoms.get_positions()
            dist = ((p1-p0)*(p1-p0)).sum(axis=1).max()
            if dist<=1e-4**2:
                break

        atoms.set_positions(ml_atoms.get_positions())

    def check(self):

        if self.nsteps == 0:
            self.e0 = self.atoms.get_potential_energy(
                force_consistent=self.force_consistent)
            self.p0 = self.atoms.get_positions()
            self.count = 0
        else:
            e = self.atoms.get_potential_energy(
                force_consistent=self.force_consistent)
            if e < self.e0:
                self.e0 = e
                self.p0 = self.atoms.get_positions()
                self.count = 0
            elif self.count < 30:
                self.count += 1
            else:
                raise RuntimeError('A descent model could not be built')


