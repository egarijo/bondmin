from bondmin.optimize import BondMin

from ase.build import bulk
from ase.calculators.emt import EMT

atoms = bulk('C', 'diamond')
atoms.rattle(0.1)
atoms.set_calculator(EMT())

opt = BondMin(atoms)
opt.run(0.05)
