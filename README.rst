Bayesian optimization package for atomic structures:
====================================================

Paper
-----

Coming soon ...


Installation
------------
Add ``~/bondmin`` to your $PYTHONPATH environment variable

Use
---

Use it in place of your favourite ASE optimizer!

>>> from bondmin import Bondmin
>>> from ase import Atoms

>>> atoms = Atoms('H2', positions = [[0,0,0],[0,0,1.2]])
>>> opt = Bondmin(atoms, trajectory = 'mytrajfile.traj', logfile='mylogsgohere.log')
>>> opt.run(fmax=0.05) 

